## knb-lter-cap.712

### dataset publishing: Tempe Town Lake datasonde

This repository contains code and materials related to publishing data from the
on-going, near-continuous monitoring of water quality in the Tempe Town Lake by
way of a Eureka datasonde.

#### knb-lter-cap.712.2 2024-11-26

- data refresh